<?php

namespace Drupal\disable_login_by_domain;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a service to determine whether current hostname is valid.
 */
class HostStatus {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a HostStatus object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Drupal config factory.
   */
  public function __construct(RequestStack $request_stack, ConfigFactoryInterface $config_factory) {
    $this->requestStack = $request_stack;
    $this->configFactory = $config_factory;
  }

  /**
   * Determines whether the user is on an allowed domain/hostname.
   *
   * @return bool
   *   Returns false if the user is on a disallowed domain, but true otherwise.
   */
  public function isAllowedDomain():bool {
    $domains = $this->configFactory->get('disable_login_by_domain.settings')->get('domains');
    $current_domain = $this->requestStack->getCurrentRequest()->getHost();

    // The asterisk/* is a wildcard to block all domains.
    if (in_array('*', $domains)) {
      return FALSE;
    }

    return !in_array($current_domain, $domains);
  }

}
