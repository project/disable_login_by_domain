<?php

namespace Drupal\disable_login_by_domain\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides domain-based access check for the user login routes.
 */
class DisableLoginPageRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach (['user.login', 'user.login.http'] as $route_name) {
      $route = $collection->get($route_name);
      $route->setRequirement('disable_login_by_domain_page_access_check', 'TRUE');
    }
  }

}
