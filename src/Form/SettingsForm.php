<?php

namespace Drupal\disable_login_by_domain\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides settings form for disable_login_by_domain.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disable_login_by_domain_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['disable_login_by_domain.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('disable_login_by_domain.settings');
    $domains_list = $config->get('domains');

    $form['domains'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disallowed domains'),
      '#description' => $this->t('Enter each disallowed domain on a new line.
        Users will not be allowed to log in if the site is using one of these
        domains.<br>Use <em>*</em> to disallow all domains.'),
      '#default_value' => implode("\n", $domains_list),
    ];
    $form['hijack_login_action'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hijack login attempts'),
      '#description' => $this->t("If checked, this module will log out a user
        immediately if they log in on a disallowed domain (assuming they have
        bypassed the other login preventions in this
        module).<br><strong>Note:</strong> Do not enable this if you allow users
        to log in outside of Drupal's login form. For example, if you want users
        to log in with single sign-on (SSO) instead of the traditional login
        form, then this option will block them from logging in with SSO."),
      '#default_value' => $config->get('hijack_login_action'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $domains_str = $form_state->getValue('domains');
    $domains_list = array_map('trim', explode("\n", $domains_str));

    $this->config('disable_login_by_domain.settings')
      ->set('domains', $domains_list)
      ->set('hijack_login_action', $form_state->getValue('hijack_login_action'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
