<?php

namespace Drupal\disable_login_by_domain\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\disable_login_by_domain\HostStatus;

/**
 * Provides domain-based access check for user login routes.
 */
class PageAccessCheck implements AccessInterface {

  /**
   * The host status service.
   *
   * @var \Drupal\disable_login_by_domain\HostStatus
   */
  protected $hostStatus;

  /**
   * The Drupal account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructor for PageAccessCheck.
   *
   * @param \Drupal\disable_login_by_domain\HostStatus $host_status
   *   The host status service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Drupal account.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Drupal config factory.
   */
  public function __construct(HostStatus $host_status, AccountProxyInterface $current_user, ConfigFactoryInterface $config_factory) {
    $this->hostStatus = $host_status;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
  }

  /**
   * Grants access if both the hostname is allowed and the user is anonymous.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The Drupal user account.
   * @param \Drupal\Core\Routing\RouteMatch $route_match
   *   The route which is attempted to be accessed.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access results.
   */
  public function access(AccountInterface $account, RouteMatch $route_match) {
    $settings = $this->configFactory->get('disable_login_by_domain.settings');

    if (
      $this->currentUser->isAuthenticated()
      || !$this->hostStatus->isAllowedDomain()
    ) {
      return AccessResult::forbidden()->addCacheableDependency($settings);
    }

    return AccessResult::allowed()->addCacheableDependency($settings);
  }

}
