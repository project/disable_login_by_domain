<?php

namespace Drupal\disable_login_by_domain\EventSubscriber;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\disable_login_by_domain\HostStatus;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Disables the user login block when placed in a Layout Builder section.
 */
class DisableUserLoginBlock implements EventSubscriberInterface {

  /**
   * The host status service.
   *
   * @var \Drupal\disable_login_by_domain\HostStatus
   */
  protected $hostStatus;

  /**
   * Drupal config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructor for DisableUserLoginBlock.
   *
   * @param \Drupal\disable_login_by_domain\HostStatus $host_status
   *   The host status service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Drupal config factory.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The Drupal renderer service.
   */
  public function __construct(HostStatus $host_status, ConfigFactoryInterface $config_factory, RendererInterface $renderer) {
    $this->hostStatus = $host_status;
    $this->configFactory = $config_factory;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];

    if (!class_exists('\Drupal\layout_builder\LayoutBuilderEvents')) {
      return $events;
    }

    // Sets to lower priority than
    // \Drupal\layout_builder\EventSubscriber\BlockComponentRenderArray so
    // that it runs after it, in order to block access to its render element.
    $events[LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY] = [
      'afterBuildCheckAccess',
      90,
    ];

    return $events;
  }

  /**
   * Disables the user_login_block when placed by Layout Builder.
   *
   * @param \Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent $event
   *   The section component render event.
   *
   * @see \Drupal\layout_builder\EventSubscriber\BlockComponentRenderArray
   */
  public function afterBuildCheckAccess(SectionComponentBuildRenderArrayEvent $event) {
    $block = $event->getPlugin();
    if (!$block instanceof BlockPluginInterface) {
      return;
    }

    if ($block->getPluginId() === 'user_login_block' && !$event->inPreview()) {
      $build = $event->getBuild();

      if (!$this->hostStatus->isAllowedDomain()) {
        $build['#access'] = FALSE;
      }

      // @todo This cache dependency does not seem to work. To work around this,
      // you need to flush caches after changing disable_login_by_domain
      // settings.
      $settings = $this->configFactory->get('disable_login_by_domain.settings');
      $this->renderer->addCacheableDependency($build, $settings);

      $event->setBuild($build);
    }
  }

}
