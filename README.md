## Introduction

This module tries to prevent users from logging in on certain domain names.

_Warning_: This module is not designed as a security tool; instead, it is
intended for convenience. See both the use case and security considerations
below.

## Example use case

Here's an example use case. Imagine that you use a CDN for public traffic to
your Drupal site, at URL https://www.example.com. Public traffic users are not
supposed to log into Drupal; only your internal staff can log in (e.g., to edit
pages).

Your staff uses the "origin" URL to log into Drupal, at
https://manage.example.com. This URL is only available behind a VPN.

For this reason, no one should be allowed to log into Drupal on the public/CDN
domain name. They shouldn't even be allowed to try to log in.

This module helps to achieve that goal. It blocks the user login forms when they
are accessed on the public domain name.

## Installation and configuration

1. Install like any other module
1. Manage the settings at `/admin/config/people/disable-login-by-domain`

## Features

These are the ways this module prevents user login attempts:

1. Disables the _User login_ block
    - Prevents access to this block in `disable_login_by_domain_block_access()`.
    - If this block is placed by Layout Builder, then it gets disabled by
      `\Drupal\disable_login_by_domain\EventSubscriber\DisableUserLoginBlock`.
1. Disables the user login form itself
    - If someone embeds the user login form into a custom block, this module
      will remove essential elements from the form, rendering it useless.
    - See `disable_login_by_domain_form_user_login_form_alter()`.
1. Disables the login page (`/user/login`).
    - See `\Drupal\disable_login_by_domain\Access\PageAccessCheck`
1. Immediately logs out a user if that user somehow finds a way to log in.
    - See `disable_login_by_domain_user_login()`

## Security considerations

This module uses the `Host`/`X-Forwarded-Host` request header to determine the
site's domain name. This header could be overridden by a malicious user,
allowing them to circumvent the protections provided by this module. To do this,
they could send a value that is both 1) not a blocked domain and 2) is in the
trusted host list (assuming that list is set; if not set, do it ASAP).

To help protect against this sort of attack, set Drupal's [Trusted Host
Settings](https://www.drupal.org/docs/getting-started/installing-drupal/trusted-host-settings).
Setting this list does not guarantee an attacker cannot bypass this module, but
does make it harder.
